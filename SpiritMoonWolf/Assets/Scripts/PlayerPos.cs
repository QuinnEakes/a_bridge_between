using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPos : MonoBehaviour
{
    private GameMaster gm;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();

        if (gameObject.tag == "Player")
        {
            transform.position = gm.lastCheckPointPos;
        }

        if (gameObject.tag == "Wolf")
        {
            transform.position = gm.lastCheckPointWolf;
        }
    }

    private void Update()
    {
        /*
        if(Input.GetKeyDown(KeyCode.LeftAlt))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        */
    }

    private void OnTriggerEnter2D(Collider2D other)//Checking for a collision
    {
        if (other.CompareTag("Death"))//Looks for an item with this tag. This one is used for targets
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
