using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    //Reference to the game master
    private GameMaster gm;

    private void Start()
    {
        //Set gm to the game object with the GM tag and its script components
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>(); 
    }

    //Gets called when the checkpoint is collided with
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player")) //Checks if the thing colliding with the checkpoint is the Player
        {
            gm.lastCheckPointPos = transform.position; // sets the last checkpoint pos to current pos
        }

        if (other.CompareTag("Wolf")) //Checks if the thing colliding with the checkpoint is the Player
        {
            gm.lastCheckPointWolf = transform.position; // sets the last checkpoint pos to current pos
        }
    }
}
