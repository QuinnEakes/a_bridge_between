using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WolfGame : MonoBehaviour
{
    public GameObject Wall; //Getting the specific wall
    public DoorOpen doorOpen; //Public for the script.
    public GameObject outputCan;
    public Text Output;
    public string[] passcode; //Apparently arrays are done this way
    string textUserPasscodeInput = "";
    string passcodeText = "";
    public float timerStartTime = 5.00f;
    public bool startTimer;
    public bool retry;

    public AudioSource howl;

    float timerStartTimeTemp = 5.00f;

    void Start()
    {
        startTimer = false;
        timerStartTimeTemp = timerStartTime;
        passcode = new string[5];//Mustmake new array to give length.
        outputCan.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        UserPasscodeInput();
        Timer();
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("Wolf"))//Looks for an item with this tag. This one is used for targets
        {
            outputCan.SetActive(true);
            GenerateNewPattern();
            retry = true;
            startTimer = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        if (other.CompareTag("Wolf"))//Looks for an item with this tag. This one is used for targets
        {
            outputCan.SetActive(false);
            retry = false;
            textUserPasscodeInput = "";
            timerStartTime = timerStartTimeTemp;
        }
    }
    // Creates a new pattern of 
    void GenerateNewPattern()
    {
        for (int i = 0; i < passcode.Length ; i++)
        {
            int number = Random.Range(0, 24);

            switch (number)
            {
                case 0:
                    passcode[i] = "a";
                    break;
                case 1:
                    passcode[i] = "b";
                    break;
                case 2:
                    passcode[i] = "c";
                    break;
                case 3:
                    passcode[i] = "d";
                    break;
                case 4:
                    passcode[i] = "f";
                    break;
                case 5:
                    passcode[i] = "g";
                    break;
                case 6:
                    passcode[i] = "h";
                    break;
                case 7:
                    passcode[i] = "i";
                    break;
                case 8:
                    passcode[i] = "j";
                    break;
                case 9:
                    passcode[i] = "k";
                    break;
                case 10:
                    passcode[i] = "l";
                    break;
                case 11:
                    passcode[i] = "m";
                    break;
                case 12:
                    passcode[i] = "n";
                    break;
                case 13:
                    passcode[i] = "o";
                    break;
                case 14:
                    passcode[i] = "p";
                    break;
                case 15:
                    passcode[i] = "q";
                    break;
                case 16:
                    passcode[i] = "r";
                    break;
                case 17:
                    passcode[i] = "s";
                    break;
                case 18:
                    passcode[i] = "t";
                    break;
                case 19:
                    passcode[i] = "u";
                    break;                
                case 20:
                    passcode[i] = "v";
                    break;
                case 21:
                    passcode[i] = "w";
                    break;
                case 22:
                    passcode[i] = "y";
                    break;
                case 23:
                    passcode[i] = "z";
                    break;
            }

            UpdatePasscodeText();
        }
    }

    void UpdatePasscodeText()
    {
        string text = "";

        for (int i = 0; i < passcode.Length ; i++)
        {
            text += passcode[i];
        }
        
        passcodeText = text;
        Output.text = "Type These Letters: " + text.ToUpper();
    }

    void UserPasscodeInput()
    {
        if(retry)
        {
            string temp = Input.inputString;

            if(Input.GetKey(KeyCode.UpArrow) == false && Input.GetKey(KeyCode.LeftArrow) == false && Input.GetKey(KeyCode.RightArrow) == false && Input.GetKey(KeyCode.DownArrow) == false)
            {
                if(temp.Length == 1 && textUserPasscodeInput.Length < passcode.Length && temp != " " && temp != "" && temp != "\n" && temp != "\r")
                {
                    Debug.Log(temp);
                    textUserPasscodeInput += temp.ToLower();
                }
            }
                
            if(textUserPasscodeInput.Length == passcode.Length)
            {
                CheckPasscode();
            }
        }
    }

    void CheckPasscode()
    {
        if(passcodeText != textUserPasscodeInput)
        {
            GenerateNewPattern();
            textUserPasscodeInput = "";
            passcodeText = "";
            retry = true;
        }
        else
        {
            Debug.Log("WIN");
            howl.Play();
            doorOpen.GetComponent<DoorOpen>().open = true;
            textUserPasscodeInput = "";
            passcodeText = "";
            retry = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    void Timer()
    {
        if (timerStartTime > 0 && startTimer)
        {
            timerStartTime -= Time.deltaTime;
        }
        else if (timerStartTime <= 0)
        {
            startTimer = false;
            timerStartTime = timerStartTimeTemp;
            outputCan.SetActive(false);
        }
    }
}
