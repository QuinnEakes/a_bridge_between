using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScript : MonoBehaviour
{
     
    public void PlayGame()
    {
        // Loads the scene directly after the current scene in the build settings
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        Debug.Log("Scene Loading");
    }

    // use this for private variables that you want in the Unity editor
    [SerializeField] Slider volumeSlider;

    void Start()
    {
        // If the player doesn't have the musicVolume key, then it will make a new one
        // Then it calls LoadVolume
        if(!PlayerPrefs.HasKey("musicVolume"))
        {
            PlayerPrefs.SetFloat("musicVolume", 1);
            LoadVolume();
        }
        // Otherwise, it calls LoadVolume
        else
        {
            LoadVolume();
        }
    }

    // Quits the game, but not in the editor
    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quitting");
    }

    public void ChangeVolume()
    {
        // Sets the AudioListener value equal to the slider's value
        AudioListener.volume = volumeSlider.value;
    }

    private void LoadVolume()
    {
        // Sets the volumeSlider value equal to the value set in PlayerPrefs
        volumeSlider.value = PlayerPrefs.GetFloat("musicVolume");
    }

    private void SaveVolume()
    {
        // Sets the musicVolume value in PlayerPrefs
        PlayerPrefs.SetFloat("musicVolume", volumeSlider.value);
    }
}
