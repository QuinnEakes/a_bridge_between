using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    public GameObject pausePanel;
    bool isPaused = false;

    // Update is called once per frame
    void Update()
    {
        // Pressing escape should pause and upause the game
        if (Input.GetKeyDown(KeyCode.Escape) & isPaused == false) 
        {
            Pause();
        }
        else if(Input.GetKeyDown(KeyCode.Escape) & isPaused == true)
        {
            Resume();
        }
    }

    // Pauses the game by stopping time
    // then activates the pause menu
    public void Pause()
    {
        Time.timeScale = 0;
        isPaused = true;
        pausePanel.SetActive(true);

        Debug.Log("game paused");
    }

    // Resumes the game by making time go again
    // then deactivates the pause menu
    public void Resume()
    {
        Time.timeScale = 1;
        isPaused = false;
        pausePanel.SetActive(false);

        Debug.Log("game resumed");
    }

    // Quits the game in the build
    public void Quit()
    {
        Application.Quit();
        Debug.Log("Quitting");
    }
}
