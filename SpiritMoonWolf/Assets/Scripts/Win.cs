using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour
{
    public GameObject winCanvas;
    public GameObject player;
    public GameObject wolf;
    public float timerStartTime;
    float timerStartTimeTemp;
    private void Start() 
    {
        timerStartTimeTemp = timerStartTime;
    }
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("Wolf") || other.CompareTag("Player"))
        {
            winCanvas.SetActive(true);
            Destroy(player.GetComponent<PlayerMovement>());
            Destroy(wolf.GetComponent<PlayerMovement>());
        }
    }

    private void OnTriggerStay2D(Collider2D other) 
    {
        if (other.CompareTag("Wolf") || other.CompareTag("Player"))
        {
            Timer();
        }
    }


    void Timer()
    {
        if (timerStartTime > 0)
        {
            timerStartTime -= Time.deltaTime;
        }
        else if (timerStartTime <= 0)
        {
            SceneManager.LoadScene("Title");
        }
    }
}
