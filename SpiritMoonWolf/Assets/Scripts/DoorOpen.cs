using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour
{
    //public Transform destination; //Final resting place for the collider.
    public bool open;
    //public GameObject doorCollider;
    // Start is called before the first frame update
    void Start()
    {
        open = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (open == true)
        {
            Destroy(gameObject);
            //doorCollider.transform.position = destination.position; //Sets the position of the collider to the open position.
        }
    }
}
