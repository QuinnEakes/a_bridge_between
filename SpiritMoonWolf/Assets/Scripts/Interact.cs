using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{

    public GameObject Wall; //Getting the specific wall
    public DoorOpen DO; //Public for the script.

    // Start is called before the first frame update
    void Awake()
    {
        DO = Wall.GetComponent<DoorOpen>();//Getting the script
        DO.open = false; //ensuring the door is defaulted to    false.
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter2D(Collider2D other)//Checking for a collision
    {
        if (other.CompareTag("Bullet"))//Looks for an item with this tag. This one is used for targets
        {
            DO.open = true; //telling a script it is allowed to open a door.
            Debug.Log("DoorOpened");
            Destroy(gameObject);
        }
    }
        private void OnTriggerStay2D(Collider2D other)//Checking for a collision
    {
        if (other.CompareTag("Bullet"))//Looks for an item with this tag. This one is used for targets
        {
            DO.open = true; //telling a script it is allowed to open a door.
            Debug.Log("DoorOpened");
        }

        if (other.CompareTag("Player"))//Looks for an item with this tag. This one is used for switches.
        {
            Debug.Log("Entered2");
            if(Input.GetKey(KeyCode.Z)/* || DO.open == false*/)//checking for button press
            {
                Debug.Log("Z");
                DO.open = true; //telling a script it is allowed to open a door.
                Debug.Log("DoorOpened");
            }
        }

    }
}
