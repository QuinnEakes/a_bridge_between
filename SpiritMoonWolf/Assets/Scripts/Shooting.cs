using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{

    public float speed;
    public Transform firePoint;
    public GameObject bulletPrefab;

    public GameObject aim;

    public AudioSource soundEffect;




    // Start is called before the first frame update
    void Start()
    {
        aim.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            aim.SetActive(true);
        }
        if (Input.GetKeyUp(KeyCode.X))
        {
            aim.SetActive(false);
            Shoot(); //Calling of shoot function.
            //aim.SetActive(false);
            
        }
    }

    void Shoot() //Function that handles our firing.
    {
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);//Spawns a bullet based on our prefab based on the position and rotation for the firepoint.
        soundEffect.Play();//Plays the sound.
        
    }
}
