using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 10f;
    public Rigidbody2D rb;

    // Update is called once per frame
    void Update()
    {
        rb.AddForce(transform.right * speed);


        //rb.velocity = transform.right * speed; //Tells the rigidbody to move right according to the speed.

        Destroy(gameObject, 10); //destroys gameobject with thio
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);
    }
}
