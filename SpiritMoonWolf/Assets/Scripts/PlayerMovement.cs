using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerMovement : MonoBehaviour
{
    public Animator animator;
    //Lets you assign the animator in the Unity Editor.
    private Rigidbody2D rb;
    //Limit the number of Jumps a player can do.
    public int jumpLimit = 2;
    //Tracks the amount of jumps the player has done
    private int jumpCount = 0;

    //Checks if player is grounded
    bool isGrounded;
    //Location of the ground
    public Transform groundCheck;
    //Get the layer of object
    public LayerMask groundLayer;
    public float jumpValue = -14f;


    Vector3 posVector; //player
    public float speed = 2;
    public Transform returnPos;
    private bool isFacingLeft = false;

    // Start is called before the first frame update
    void Start()
    {
        //Gets Rigidbody component to rb
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Creates a small circle at the position to be able to check if the player is grounded or not
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);

        //Direction in the X axis
        float dirX = Input.GetAxisRaw("Horizontal");
        //Movement ~ Move the player in the left or right direction based on A or D
        rb.velocity = new Vector2(dirX * 7f, rb.velocity.y);

        animator.SetFloat("Speed", Mathf.Abs(dirX));

        if (dirX < 0 && isFacingLeft == false)//Checks if we are facing left based off of previous movement.
        {
            transform.Rotate(0f, 180f, 0f); //Flips character
            isFacingLeft = true;
        }
        if (dirX > 0 && isFacingLeft == true)//Checks if we are facing right based off of previous movement.
        {
            transform.Rotate(0f, 180f, 0f);//Flips back to the right
            isFacingLeft = false;
        }

        //Jump Mechanic
        if (jumpCount < jumpLimit && Input.GetButtonDown("Jump"))
        {
            //Applying Velocity to rigidbody 
            rb.velocity = new Vector2(rb.velocity.x, jumpValue);
            jumpCount++;
            animator.SetBool("isJumping", true);
        }

        //If the player has hit the jump limit this if runs to check if player is grounded
        if (jumpCount >= 2)
        {
            //If the player is grounded then the jump count becomes 0 and the player can jump again
            if (isGrounded)
            {
                jumpCount = 0;
                animator.SetBool("isJumping", false);
            }
        }

        if (Input.GetKey(KeyCode.E)) //Checking for E press
        {
            transform.position = Vector2.MoveTowards(transform.position, returnPos.position, speed * Time.deltaTime); //Movement towards the human player.
        }

    }//End Update
}
