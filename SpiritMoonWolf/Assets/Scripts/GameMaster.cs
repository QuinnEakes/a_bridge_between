using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    //Create an instance of Checkpoints
    private static GameMaster instance;
    //Position of last checkpoint
    public Vector2 lastCheckPointPos;
    public Vector2 lastCheckPointWolf;

    private void Awake()
    {
        //Checks if there is already in Instance if not then it assigns this to the instance
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance); //So the object doesn't destory itself between scenes or resets variables
        }
        else
        {
            Destroy(gameObject); //If there was an instance already destroy the game object
        }
    }
}
